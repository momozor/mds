python3.6 and mypy

Quick example
-------------

## meow.py

    def meow(name: str) -> str:
        return 'meowwwww {}'.format(name)

    name: str = 'jack'
    print(meow(name))

then run

    mypy meow.py

Tags: python, python3, mypy, static checker, static typing, safety, best practices
