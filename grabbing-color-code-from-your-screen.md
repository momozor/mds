Grabbing color code from your screen

*grabc* is a fantastic tool to get color value (RGB and hex) from your computer screen using your mouse pointer. It do one job and only, and do it very well. Great for front end designers who wish to use the color they saw on the screen but not knowing the actual hex or RGB value of the color.

Gnome's color chooser is a good alternative to grabc if you want more than a color picker. It gives you a lot of options to play with colors and its values.

Tags: software, color chooser, linux, color
