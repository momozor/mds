Compiler? No, it is an interpreter.

I've decided to use Racket due to the included lex and parse modules in the distribution, its documentations and a lot of example code to be learned from.

Oh, and I think want to start with building an interpreter first. Messing up with the low level abstraction for instance, C and assembly is not my goal. They will be too distracting for my first project for this kind.



Tags: compiler, programming, development, programming languages
