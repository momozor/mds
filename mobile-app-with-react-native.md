Mobile app with React Native?

These days, people will more often than not use their smartphone to do pretty much everything that doesn't require much typing or pixel manipulation. I think it is time to venture the mobile ecosystem with React Native. It sounds promising.


Tags: react, mobile, smartphone, android, native, application
