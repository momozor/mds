Compiler? No, it is an interpreter 2.

[Original](https://faraco.github.io/compiler-no-it-is-an-interpreter.html)

Instead of Racket, I'm now using Chicken Scheme instead. The reason is due to very light memory requirements. I don't want my dev environment stop working for the Racket GC frequent collection runs. Thus, I already have a half working custom lexer for the simple Scheme language. I called it Chilli for now.

## Why not use yacc and lex like in the original article?

I found that full core Scheme is actually made out of 5 keywords and 8 syntactic forms. I think learning yacc and lex will be redundant if I don't know how parsing works in the long term anyway. Furthermore, because of tiny amount of Scheme syntax, parsing should be easier. 

## Ideal Features?

It is a toy interpreter but it should allow the programmer to use itself to program another scheme interpreter/compiler. It should be Turing-complete (in this case, we use Lambda Calculus theory). No macros and tail-call optimization will be supported in the first alpha release. This is to avoid the complexity of the codebase so I can refactor the code over time and make it more rigid.

Tags: compiler, interpreter, programming, development, chilli, scheme, lisp
