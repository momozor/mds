Perl and Type Constraints?

Perl (in particular Perl 5) by default is notorious with its lack of strong (weakly typed but this is minor) and static type checking in the compile-time phase. This can lead to silly annoying type related bugs to lurk in your codebase (most bugs are silly, FYI). To combat this issue, I do recommend to use the *crawfish* module set.

1. Function::Parameters
    - Restrict types of your arguments being **passed to** a function or method.

2. Return::Type
    - Restrict types of your function or method **return** value.

Above modules isn't the answer to make Perl 5 'static type checked'. They are not embedded into the Perl's grammar and
sometimes, you might found a few mistaken *checks* passing through the constraints.

Eitherway, it works for simple stuff (as far as I can tell from a 5 minutes usage) and maybe do as fine for large codebases.

To conclude, who cares about what I said? Try it for yourself and make up your mind.


Tags: perl, perl5, type checking, type, static, weak, strong, checking, constraints, development, programming
