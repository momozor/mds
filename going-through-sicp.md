Going through SICP

Okay. I should be implementing my programming language interpreter right now but instead I'm working through SICP book. Why?

Because I have to. The SICP book contains hundreds of examples and quality exercises and many Lisp related stuff. For me to implement a Lisp is I should read *what* makes a Lisp first? Sounds familiar?

Tags: lisp, scheme, language, compiler, interpreter, theory, science
