My journey on making a compiler

I was interested in the magic of compiler (and interpreter) and how it works. Just imagine how such a tool where you can install into your own smartphone, able to produce behemoths being used by billions of people, in the critical mission projects including satellite image recognition software, wave transmitter, controller for various nuclear facilities, heart rate monitor, heat seeking programs, or a website that connects countless of people with each other over the internet.

It is a dream to make one, and I wish to bring it into the reality. The journey of my adventure began now..

*Semut-semut mampu membina sarang di bawah tanah yang teramat besarnya.*

Tags: compiler, programming, development, software, interpreter, theory, science
