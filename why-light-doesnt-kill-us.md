Why light doesn't kill us?

Have you ever asked this question? Why light (in the sense of sunlight and artificial bulb light) doesn't kill us if they move from point A to B so quickly (remember, E=mc^2)? They key is **photon** and **mass** or in this case, **massless**.

Tags: physics, science, astrophysics, light
