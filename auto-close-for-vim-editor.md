Auto close for vim editor

## The pain to manually do the tedious

I get it. Vim is fantastic for quick-and-dirty to multi tiered code editing but the stock vim is PITA to work with.

I recommend to use my simple vim setup to increase your happiness and productivity with vim.

Begin the setup by appending the code below to your .vimrc file (run ***mkdir ~/.vimundo*** first for the undo/revert history feature):

    filetype plugin indent on
    set expandtab
    set autoindent
    set number
    set tabstop=4
    set shiftwidth=4
    colorscheme monokai
    set undofile
    set undodir=/home/your_user_name/.vimundo/
    set mouse=a
    set cmdheight=2
    set hlsearch
    set incsearch
    set magic
    set nobackup
    set nowb
    set noswapfile

and download and install the amazing [jiangmiao's](https://github.com/jiangmiao/auto-pairs) vim auto-pairs plugin:

    wget -c https://raw.githubusercontent.com/jiangmiao/auto-pairs/master/plugin/auto-pairs.vim -O ~/.vim/plugin/

Next, proceed to install the [monokai](https://github.com/sickill/vim-monokai) colorscheme (it's my preference but it is okay too to use any colorscheme you want, but make sure to change value at the line with 'colorscheme' in your .vimrc file):

    wget -c https://raw.githubusercontent.com/sickill/vim-monokai/master/colors/monokai.vim -O ~/.vim/colors

Enjoy your boosted productivity with vim! :)
    
Tags: vim, setup, configuration, software, development, text-editor, productivity 
