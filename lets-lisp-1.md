Let's Lisp 1

## To read this, will save you a large amount of time from being wasted
Let me start to acknowledge this series will not be a list of tutorials. This is more as a scribble/black board for me to chart and write notes on what I've learn from Lisp. However, it doesn't have be that way. I may occasionally do present the notes in a form of tutorial/manual, but hey let's see that in the coming articles.

### Topic - *Atoms* in Lisp
Atoms just like in physical science means in general concensus, ***indivisible***. They cannot be broken to something smaller just like if you cut yourself in half and then you will die. There are *two* type of atoms in Lisp. One is a constant numeral (numbers) and another, is a literal constant (eg, unquoted text). They both can be combined to form a new distinct atomic expression or compound expression (will talk about this later).

#### Example of *Atoms* in Lisp
```
    12345
    3.4E92
    COUNTRY
    SOUTH
    NORTH
    DIAMOND
    META
```

Tags: lisp, common lisp, lets-lisp-series, programming, tutorial, manual, development, algorithm
