Making a custom lexer

I've learn that using any kind of parser generator and combinator (which both of these includes lexing) are pretty much waste of time. You'll have to deal with learning the predefined rules of the generator and your target language grammars at the same time. This is especially terrible if this is your first time parsing a context-free (or mostly) programming language.

Tags: lexer, compiler, parser, software, programming
