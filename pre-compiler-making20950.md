Pre-compiler making

![business-1839191_1280](https://user-images.githubusercontent.com/24475030/41192434-07aefff0-6c30-11e8-9de2-e8c145a4d5d2.jpg)

Dilemma is what haunting me at the time I'm writing this very article. I need to decide what programming language should I use to implement the compiler and what programming language the compiler targets.

> Good and better in my **opinion**.

I have found several good programming languages which gonna implement the compiler (source language):

* OCaml
    - Static typing (and better,type inference)
    - Good package manager
    - Immutability of variables by default
    - Good support of OOP and functional paradigm as a first class citizen
    - Pattern matching and algebraic data type (I heard these two are good for compiler making, we'll see)
    - Came with ocamllex and ocamlyacc (yeah!) at least on Ubuntu 16.04 system installation
    - And it compiles! (ocamlopt always links all Ocaml code statically except for C libraries, so distribution with single fat binary might not be a big problem at all, but again this compiler is for educational purpose)

* Scheme
    * Freedom, flexibility, simplicity.
    * Syntax imitates tree data structure.
    * Favors functional programming
    * Tail-call optimization (recursionists haven)
    * Simplicity
    - Another one for simplicity (that wasn't even enough)        
    - Macros ease of use makes playing with syntax very flexible (and this is probably a BIG advantage for compiler making).


    - Chicken Scheme
        - csc compiles to native code (using C compiler!)
        - Very light for a language and environment
        - Good package manager and ok amount of libraries
        - Since it uses C compiler to generate native code, I can always tell the compiler to stop at C compilation before going into assembly code generation.
        - Opens up to gigantic area of C libraries but might not be a very good idea for compiler making (simplicity, anyone?).

    - Racket
        - A language & platform designed for language research (isn't this near perfect for compiler making?)
        - Fantastic and specialized but memory extensive IDE.
        - Big enough community, excellent documentation, batteries included libraries and amazing package manager!
        - Comes with a tool to package your software as a single folder/directory + dependencies (or even a single static binary?)
        - Interpreted?
        - Typed Racket! (whew! Typed Lisp sounds cool and 'safer')

    - Guile
        - Lovely GNU Guile documentation.
        - Ubiquitous on GNU/Linux (as long as they are installed)
        - Emacs and Guile? Yes!
        - Amazing C-to Guile and Guile-to-C interface.
        - However, this implementation doesn't have significant advantage for compiler making in my case.
        - Other than GNU documentation, you have to work to figure out stuff on your own.
        - Small if not tiny community (this is very unfortunate with the goal and the official status of the project, hope this will change soon)


* Haskell
    - Most of the OCaml advantage excepts for pragmatism of it.
    - I found it is quite too advanced for what I want to do even f other programmers do consider this as the best one for the job.
    - I want something out of the box with pragmatism not on solely theory and I don't have time to focus on only Haskell.
    - Good platform, decent compiler and package manager.
    - Big community consisting engineers, mathematicians and computer scientists (I should visit this community regardless to learn more!)
    - Official documentation is good but I don't think it beats Racket in this field.

* Ruby
    - Great community
    - Splendid support for OOP and functional paradigm!
    - Too focused on web development
    - Metaprogramming? (lispy much?)
    - Perl pragmatism (I'd still enjoy Perl 5 and very productive with it in 2018)
    - Large amount of libraries and documentations.
    - The only language after Lisp that I consider duck typing as an advantage (not even Perl 5).
    - Stable environment and platform

* Python 3
    - I adore the syntax minimalism.
    - I hate the python2 vs python3 issue for existing
    - Great community
    - Overall a good platform for both web, science and desktop application development.
    - Quirky functional paradigm support.
    - I dislike Python's OOP compared to Ruby.
    - I'm most familiar with this language but find it is too restraining and when I want to play with syntax (which I expect a lot for the project).
    - No specialization much except for data science and some sort for A.I (which I'm not interested at all for data science and not planning to involve myself with it anytime soon).
    - Newbies everywhere, expect cargo-cult zealots and random irrational Python fanboys. (why I'm ranting this in here?)

* C
    - Probably a very bad idea because I have to do janitorial work while programming. (duh)
    - Simplicity.
    - Too verbose! (argh, worse idea while learning to make a compiler!)
    - Library is okay to build the wheels, but horrible to build a car without reinventing the wheel. (I'm making the factory for cars, so yeah.)
    - Fantastic documentation (there is even a 'make a lisp interpreter in C' tutorial)
    - Too raw! Medium rare is the least I should get.
    - I'll grow a beard before I even finish tinkering with evaluation!
    - With other better options, not a good idea to for this very job.

* C++
    - Segmentation fault (monstrosity dumped).

* Golang
    - Simplicity.
    - Garbage collector.
    - By default, compiles to static binary.
    - For me, is C v2.0
    - Verbose (in the name of repetitive spaghetti monster!)
    - Lightweight syntax
    - Good package manager, ok set of libraries and Google (for the best or the worst)
    - Hyped community but not sure if it will stay that way in the near future.

* Rust
    - C++ mentally abused by Haskell step-sister.
    - Comes with chastity belt to avoid you from making stupid 'mistakes'.
    - Safe
    - Safe
    - Monstrolegance?
    - Good cousin with Ada.
    - Always joked about Arianne, her lover to Ada.
    - Ada got mad and left her.
    - Rust adopted by sugar daddy, Mozilla.
    - Will take your soul (do we have any?) and feed it to the picky compiler.
    - I don't know why the hell I'm typing these when it comes to Rust.

The target language (implementation language):

* Scheme
    - Simple grammar
    - Simplicity
    - Simplicity

* C
    - Ubiquitous
    - Simplicity
    - Ubiquitous
    - Compiler for new, revolutionary operating system? (coming soon in the next decade :/)

> Repetitions were intentionally done.

Final decision will be made in the next article.

Tags: programming, development, theory, science, compiler, programming languages
