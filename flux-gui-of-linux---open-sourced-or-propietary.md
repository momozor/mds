flux-gui of Linux - Open sourced or proprietary?

[flux-gui](https://github.com/xflux-gui/fluxgui)

As a supporter and an avid user of open source and free software, taking account of software freedom to both developers and users including services that I use is a habit of mine. (I also noticed about the propietary Github I'm using and recently planned to be [acquired](https://techcrunch.com/2018/06/04/microsoft-has-acquired-github-for-7-5b-in-microsoft-stock/) by Microsoft).

Taken from `flux-gui` README:
> This project -- https://github.com/xflux-gui/fluxgui -- is only concerned with the fluxgui indicator applet program, not with the underlying xflux program the indicator applet controls. 

In summary, the project claimed that the applet software is MIT licensed while the `xflux` proprietary binary file that need to be downloaded to be used to run the program is not. It is a mixed combination of proprietary and open sourced program right there.

While, this legally would not be a problem (I'm not a lawyer, so not entirely sure) this opposed my stand with free software and its values. I wish I can contribute more to the project without taking their end users computing freedom and risking them for malwares.

Any alternatives?

Yes! I'd recommend [redshift](http://jonls.dk/redshift/) and it was released under the GPL-3.0 license! It is what I use currently and loved the simplicity of the software! `redshift -O 3900` makes all the difference.

Tags: proprietary, GPL, free software, propietary, software, development, linux
