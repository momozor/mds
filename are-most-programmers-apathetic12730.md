Are most programmers, apathetic?

Can we just... for a bit moment appreciate each other's presence, efforts and careness as it is? I know some of us have to pay the bills and don't want to care about all of these things, but what about the hobbyists and those who find programming as a sheer fun? Does the enjoyment of programming had to be earned in an egotistical manner? Why we need to look down upon each other instead of empowering? Why we need to be too bureaucratic of everything we do? 

Where is the curious and lovely child you used to be?

Tags: null
